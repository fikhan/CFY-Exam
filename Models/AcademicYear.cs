﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace CFY_Exam.Models
{
    public partial class AcademicYear
    {
        public int AcademicYearId { get; set; }
        public string AcademicYearName { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        public int? Status { get; set; }
        public string FirstUserName { get; set; }
        public DateTime? FirstTimeStamp { get; set; }
        public string LastUserName { get; set; }
        public DateTime? LastTimeStamp { get; set; }
    }
}
