﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CFY_Exam.Models
{
    public partial class Semester
    {
        public int SemesterId { get; set; }
        public string SemesterName { get; set; }
        public int? SemesterType { get; set; }
        public int? AcademicYearId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? Status { get; set; }
        public string FirstUserName { get; set; }
        public DateTime? FirstTimeStamp { get; set; }
        public string LastUserName { get; set; }
        public DateTime? LastTimeStamp { get; set; }
    }
}
