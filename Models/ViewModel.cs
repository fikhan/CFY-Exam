﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CFY_Exam.Models
{
    public class ViewModel
    {
        public IEnumerable<UserGroup> UserGroups { get; set; }
        public IEnumerable<Privilege> Privileges { get; set; }
        public IEnumerable<GroupPrivilige> UserGroupPriviliges { get; set; }
    }
}
