﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CFY_Exam.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T GetById(long id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Remove(T entity);

        void Update(T entity);
    }
}
