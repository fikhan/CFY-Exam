﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CFY_Exam.Models;
namespace CFY_Exam.Controllers.Shared
{
    public class GroupPriviligeController_1 : Controller
    {

        private readonly CFYExamContext _context;

        public GroupPriviligeController_1(CFYExamContext context)
        {
            _context = context;
        }
        public IActionResult GroupPriviligeIndex()
        {
            using (var con = new CFYExamContext())
            {
                List<UserGroup> ug = _context.UserGroups.ToList() ;
                List<Privilege> priv = _context.Privileges.ToList();
                ViewBag.UserGroups = ug;
                ViewBag.Privileges = priv;
                //Console.WriteLine("I am in gpcontroller");
               //var rows = from myRow in con.UserGroups select myRow;
              //  var priv = from myRow1 in con.Privileges select myRow1;
            }
                return View();
        }
        [HttpPost]
        public void CreateGroupPrivilige(GroupPrivilige priv)
        {
            //Do something with formData
        }
    }
}
