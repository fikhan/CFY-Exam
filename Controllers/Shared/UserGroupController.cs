﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CFY_Exam.Models;

namespace CFY_Exam.Controllers.Shared
{
    public class UserGroupController : Controller
    {
        public IActionResult GroupIndex()
        {
            return View();
        }

        [HttpPost]
        public void CreateUserGroup(UserGroup gr)
        {
           

            using (var con = new CFYExamContext())
            {
                string code = gr.UserGroupCode;
                string groupname = gr.GroupName;
              
                
                DateTime currenttime = DateTime.Now;
                var ug = new UserGroup();
                ug.UserGroupCode = code;
                ug.GroupName = groupname;
                ug.Status = gr.Status;
                ug.FirstTimeStamp = currenttime;
                ug.FirstUserName = "First User Name";

                con.UserGroups.Add(ug);
                con.SaveChanges();
            }
                //Console.WriteLine("code : " + code + "groupname : " + groupname + "status : " + grstatus);

        }

        public IActionResult CreateUserwithGroup()
        {
            return View();
        }

        public IActionResult CreateUpdatewithGroup()
        {
            return View();
        }

      
    }
}
