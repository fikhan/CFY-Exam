﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using CFY_Exam.Models;
using Microsoft.AspNetCore.Mvc;


namespace CFY_Exam.Controllers.Shared
{
    public class PriviligeController_1 : Controller
    {
        public IActionResult PriviligeIndex()
        {
            return View();
        }

        [HttpPost]
        public void CreatePrivilige(Privilege priv)
        {
            using (var con = new CFYExamContext())
            {
                string privcode = priv.PriviligeCode;
                string privname = priv.PriviligeName;
                int privtype = priv.PriviligeType;
                DateTime currenttime = DateTime.Now;
                var privt = new Privilege();
                privt.PriviligeCode = privcode;
                privt.PriviligeName = privname;
                privt.PriviligeType = privtype;
                privt.Status = priv.Status;
                privt.FirstUserName = "first username";
                privt.FirstTimeStamp = currenttime;

                con.Privileges.Add(privt);
                con.SaveChanges();
            }
            //Console.WriteLine("code : " + privcode + "groupname : " + privname + "priv type : " + privtype + "status : " + status);

        }

        public IActionResult CreatePrivilgewithGroup()
        {
            return View();
        }

        public IActionResult CreateUpdatePrivilgewithGroup()
        {
            return View();
        }
    }
}
