﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CFY_Exam.Controllers.Public
{
    public class Enums
    {
        public enum GeneralStatus
        {
            [Display(Name = "Active")]
            Active = 1,
            [Display(Name = "InActive")]
            InActive = 0,
            [Display(Name = "Deleted")]
            Deleted = 2
        }
    }
}
