﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CFY_Exam.Models;

namespace CFY_Exam.Controllers
{
    public class GroupPriviligesController : Controller
    {
        private readonly CFYExamContext _context;

        public GroupPriviligesController(CFYExamContext context)
        {
            _context = context;
            
        }

        // GET: GroupPriviliges
        public async Task<IActionResult> Index()
        {
            return View(await _context.GroupPriviliges.ToListAsync());
        }

        // GET: GroupPriviliges/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupPrivilige = await _context.GroupPriviliges
                .FirstOrDefaultAsync(m => m.GroupPriviligeId == id);
            if (groupPrivilige == null)
            {
                return NotFound();
            }

            return View(groupPrivilige);
        }

        // GET: GroupPriviliges/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GroupPriviliges/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupPriviligeId,PriviligesId,UserGroupId")] GroupPrivilige groupPrivilige)
        {
            if (ModelState.IsValid)
            {
                _context.Add(groupPrivilige);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(groupPrivilige);
        }

        // GET: GroupPriviliges/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupPrivilige = await _context.GroupPriviliges.FindAsync(id);
            if (groupPrivilige == null)
            {
                return NotFound();
            }
            return View(groupPrivilige);
        }

        // POST: GroupPriviliges/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("GroupPriviligeId,PriviligesId,UserGroupId")] GroupPrivilige groupPrivilige)
        {
            if (id != groupPrivilige.GroupPriviligeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(groupPrivilige);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupPriviligeExists(groupPrivilige.GroupPriviligeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(groupPrivilige);
        }

        // GET: GroupPriviliges/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupPrivilige = await _context.GroupPriviliges
                .FirstOrDefaultAsync(m => m.GroupPriviligeId == id);
            if (groupPrivilige == null)
            {
                return NotFound();
            }

            return View(groupPrivilige);
        }

        // POST: GroupPriviliges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var groupPrivilige = await _context.GroupPriviliges.FindAsync(id);
            _context.GroupPriviliges.Remove(groupPrivilige);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GroupPriviligeExists(long id)
        {
            return _context.GroupPriviliges.Any(e => e.GroupPriviligeId == id);
        }

        public IActionResult GroupPriviligeAssignment()
        {
            List<UserGroup> ug = _context.UserGroups.ToList();
            List<Privilege> priv = _context.Privileges.ToList();
            var listugname = ug.Select(x => x.GroupName).ToList();
            var pivname = priv.Select(x => x.PriviligeName).ToList();
            var listid = ug.Select(x => new UserGroupNameIdMatch() { UGName = x.GroupName, id = x.UserGroupId.ToString() }).ToList();
            var privid = priv.Select(x => new PriviligeNameIdMatch() { PrivName = x.PriviligeName, idPriv = x.PriviligesId.ToString() }).ToList();
            ViewBag.UserGroupsId = listid.Select(x => new SelectListItem() { Text = x.UGName, Value = x.id });
            ViewBag.PrivilegesId = privid.Select(x => new SelectListItem() { Text = x.PrivName, Value = x.idPriv });
            return View();
        }

        [HttpPost]
        public IActionResult CreateGroupPrivilige()
        {

            return View();
        }
    }
}
